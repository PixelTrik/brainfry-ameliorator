# BrainFry Ameliorator
A Brainfuck compiler made in Rust that compiles to idiomatic C. This is my
foray when it comes to code optimization in compiler design. This is done by
making lexemes using Rust's enums and generating C code based on values stored
in each enum if it does.

But first, it checks if the brackets are valid, if not, then you brainfuck code
cannot be compiled.

# Usage
## As a Library
To use this crate as a library, you need to add dependency in `Cargo.toml` as

```
[dependencies]
brainfry_ameliorator = {git = "https://gitlab.com/PixelTrik/brainfry-ameliorator"}
```

And bring it into scope using

```
use brainfry_ameliorator;
```

## As a Binary
To use it as a binary, build and run using

    > cargo run <brainfuck-file>

It will generate an equivalent C code that will be saved in the current working
directory.
