use std::str::Chars;
use std::iter::Peekable;
use std::process::exit;

#[derive(Debug, PartialEq)]
enum Lex {
    PtrLeft(u16),
    PtrRight(u16),
    Inc(u16),
    Dec(u16),
    Write(u16),
    Read(u16),
    LoopStart,
    LoopEnd
}

pub fn gen_c_code(bf_str: String) -> String {
    if !validate_brackets(&bf_str) {
        eprintln!("Unpaired brackets found!");
        eprintln!("Your text editor can help you please stop using Notepad for this.");
        exit(1);
    }

    let lex = gen_lex(bf_str);
    get_c_code(lex)
}

fn get_c_code(lexs: Vec<Lex>) -> String {
    let mut res = vec![];
    let mut itr = lexs.iter().peekable();
    let mut spaces = 4;

    while let Some(lex) = itr.peek() {
        match lex {
            Lex::PtrRight(count) => { res.push(format!("{:width$}ptr += {};", " ", count, width = spaces)); },
            Lex::PtrLeft(count) => { res.push(format!("{:width$}ptr -= {};", " ", count, width = spaces)); },
            Lex::Inc(count) => { res.push(format!("{:width$}*ptr += {};", " ", count, width = spaces)); },
            Lex::Dec(count) => { res.push(format!("{:width$}*ptr -= {};", " ", count, width = spaces)); },
            Lex::LoopStart => {
                res.push(format!("\n{:width$}while (*ptr) {{", " ", width = spaces));
                spaces += 4;
            },
            Lex::LoopEnd   => {
                spaces -= 4;
                res.push(format!("{:width$}}}\n", " ", width = spaces));
            },
            Lex::Write(count) => {
                if count == &1 {
                    res.push(format!("{:width$}putchar(*ptr);", " ", width = spaces));
                } else {
                    res.push(format!("\n{:width$}for (int x = 0; x < {}; x++)", " ", count, width = spaces));
                    res.push(format!("{:width$}putchar(*ptr);\n", " ", width = spaces + 4));
                }
            },
            Lex::Read(count) => {
                if count == &1 {
                    res.push(format!("{:width$}*ptr = getchar();", " ", width = spaces));
                } else {
                    res.push(format!("{:width$}for (int x = 0; x < {}; x++)", " ", count, width = spaces));
                    res.push(format!("{:width$}*ptr = getchar();", " ", width = spaces + 4));
                }
            }
            
        }

        itr.next();
    }

    res.join("\n")
}

fn validate_brackets(src: &String) -> bool {
    let mut balance = 0;

    for ch in src.chars() {
        match ch {
            '[' => balance += 1,
            ']' => balance -= 1,
            _ => {},
        }
    }

    balance == 0
}

fn gen_lex(src: String) -> Vec<Lex> {
    let mut res = Vec::new();
    let mut itr = src.chars().peekable();

    while let Some(ch) = itr.peek() {
        match ch {
            '>' =>  { res.push(Lex::PtrRight(get_count('>', &mut itr))); },
            '<' =>  { res.push(Lex::PtrLeft(get_count('<', &mut itr)));  },
            '+' =>  { res.push(Lex::Inc(get_count('+', &mut itr)));      },
            '-' =>  { res.push(Lex::Dec(get_count('-', &mut itr)));      },
            ',' =>  { res.push(Lex::Read(get_count(',', &mut itr)));     },
            '.' =>  { res.push(Lex::Write(get_count('.', &mut itr)));    },
            '[' =>  { res.push(Lex::LoopStart); itr.next();              },
            ']' =>  { res.push(Lex::LoopEnd);   itr.next();              },
            _ =>    { itr.next(); },
        }
    }

    res
}

fn get_count(ch: char, itr: &mut Peekable<Chars>) -> u16 {
    let mut count = 0;

    while itr.next_if(|&cx| ch == cx).is_some() {
        count += 1;
    }

    count
}

// Unit Tests

#[test]
fn test_valid_brackets() {
    let src = "[[[]]]".to_string();
    assert_eq!(validate_brackets(&src), true);
}

#[test]
fn test_invalid_brackets() {
    let src = "[][".to_string();
    assert_eq!(validate_brackets(&src), false);
}

#[test]
fn test_c_while_loops() {
    let lex = vec![Lex::LoopStart, Lex::LoopEnd];
    let res = "    while (*ptr) {\n    }";
    assert_eq!(get_c_code(lex), res);
}

#[test]
fn test_c_code() {
    let lex = vec![Lex::PtrRight(3), Lex::PtrLeft(3)];
    let res = "    ptr += 3;\n    ptr -= 3;";
    assert_eq!(get_c_code(lex), res);
}

#[test]
fn test_ptr_moves() {
    let src = ">>><<<".to_string();
    let res = vec![Lex::PtrRight(3), Lex::PtrLeft(3)];
    assert_eq!(gen_lex(src), res);
}

#[test]
fn test_lex_count() {
    let src = ">>>".to_string();
    let mut itr = src.chars().peekable();
    assert_eq!(get_count('>', &mut itr), 3);
}
