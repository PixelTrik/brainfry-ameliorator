use brainfry_ameliorator;
use std::{env, fs, process};
use std::path::Path;
use std::io::Write;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Too few arguments! Mention a BrainF*ck file.");
        process::exit(1);
    }

    let filename = &args[1];
    let mut dst_name = Path::new(filename).file_stem().unwrap().to_str().unwrap().to_string();
    dst_name += ".c";

    let mut out_file = fs::File::create(dst_name).expect("Unable to make C file!");
    out_file.write_all(gen_c_program(filename).as_bytes()).expect("Unable to write to C file!");
}

fn gen_c_program(bf_file: &String) -> String {
    let contents = fs::read_to_string(bf_file).unwrap();

    let prog = vec![
        "#include <stdio.h>".to_string(),
        "unsigned int mem[30000];\n".to_string(),
        "int main() {".to_string(),
        "    unsigned int *ptr = mem;\n".to_string(),
        brainfry_ameliorator::gen_c_code(contents),
        "\n    return 0;".to_string(),
        "}".to_string(),
    ];

    prog.join("\n")
}
